# Task elements:
1. Create a way to fetch a list of cities with basic current weather information (min and max
temperature, weather conditions). Add options to filter cities, by their area, population or other
information.
2. Create a way to fetch a single city with extended current weather information (temperatures,
weather conditions, pressure, humidity, etc). Add options to this request to include a 5 day/3 hour
forecast.
3. Create a way to fetch current weather forecast for user provided coordinates. Add options to this
request to include a 5 day/3 hour forecast.
4. Create a way to add a city to the list of available cities for the weather forecast. City information
should contain city name, area, population (can add other fields, if desired). This information is
entered by the user. Upon insertion, a city must be assigned a city ID from the city.list.json file
(available here http://bulk.openweathermap.org/sample/).
5. Create a way to remove a city from the list of available cities.
6. Create a way to update a city information.
7. Create a way to add user. User information must consist of user name, password, first name, last
name, home location, favourite cities.
8. Create a way to update user information.
9. Create a way to remove user information.
10. Create a way to retrieve a list of users. Add options to filter users by first name, last name, favourite
cities.
11. Create a way to retrieve single user.
12. Create a way to authorize user using Basic Authorization.


# Running application with a persistent DB
1. Launch the application MySQL database:
- ``cd`` into **task-forum/Deployment** directory
- run command ``docker-compose up --build`` to start MySQL DB instance in Docker
2. Launch the SpringBoot application via command line:
- ``cd`` into **weathers** directory
- run command ``mvn compile`` to compile java classes
- run command ``mvn package`` to package the application into a .jar file
- run command ``java -jar target/weathers-api.jar -Dspring.profiles.active=ote`` to run the .jar application
- 
# Running application with an in-memory DB
1. Launch the SpringBoot application via command line:
- ``cd`` into **weathers** directory
- run command ``mvn compile`` to compile java classes
- run command ``mvn package`` to package the application into a .jar file
- run command ``java -jar target/weathers-api.jar -Dspring.profiles.active=dev`` to run the .jar application

Sorry for poor work on packaging... missing time to do that...

# Some curls by task No:
1) ```curl --location --request GET 'http://localhost:8080/conditions/basic/search?name=New&country=US'```

2) ```curl --location --request GET 'http://localhost:8080/conditions/Vilnius?withForecast=true'```

3) ```curl --location --request GET 'http://localhost:8080/conditions/byCoordinates?lon=24&lat=55'```

4) ```curl --location --request POST 'http://localhost:8080/cities/add' \
   --header 'Authorization: Basic YWRtaW46cGFzcw==' \
   --header 'Content-Type: application/json' \
   --data-raw '{
   "id": 5128581,
   "name": "New York City",
   "country": "US",
   "state": "NY",
   "coord": {
   "lon": -74.005966,
   "lat": 40.714272
   },
   "area": 1223.59,
   "population": 8419000,
   "isEnabled": true,
   "timezone": -18000
   }'```

5) ```curl --location --request DELETE 'http://localhost:8080/cities/delete/5128581' \
   --header 'Authorization: Basic YWRtaW46cGFzcw==' \
   --header 'Content-Type: application/json' \
   --data-raw '{
   "id": 5128581
   }'```

6) ```curl --location --request PUT 'http://localhost:8080/cities/update' \
   --header 'Content-Type: application/json' \
   --data-raw '{
   "id": 5128581,
   "name": "New York City",
   "country": "US",
   "state": "NY",
   "coord": {
   "lon": -74.005966,
   "lat": 40.714272
   },
   "area": 1223.59,
   "population": 8419000,
   "isEnabled": true,
   "timezone": -18000
   }'```
   
7) ```curl --location --request POST 'http://localhost:8080/users/add' \
   --header 'Content-Type: application/json' \
   --data-raw '{
   "username": "username4",
   "firstName": "firstName2",
   "lastName": "lastName2",
   "homeLocation": "homeLocation2",
   "password": "pass",
   "passwordRepeat": "pass",
   "favouriteCities": [
   {
   "id": 2960
   }
   ]
   }'```
   
8) ```curl --location --request PUT 'http://localhost:8080/users/update/profile' \
   --header 'Authorization: Basic YWRtaW46cGFzcw==' \
   --header 'Content-Type: application/json' \
   --data-raw '{
   "username": "username4",
   "firstName": "firstName1",
   "lastName": "lastName1",
   "homeLocation": "homeLocation1",
   "password": "pass2",
   "passwordRepeat": "pass2",
   "favouriteCities": [
   {
   "id": 2960,
   "name": "Ḩeşār-e Sefīd",
   "country": "IR",
   "state": "",
   "coord": {
   "lon": 47.159401,
   "lat": 34.330502
   },
   "area": null,
   "population": null,
   "isEnabled": null,
   "timezone": null
   }
   ]
   }'```

9) ```curl --location --request DELETE 'http://localhost:8080/users/delete/username4' \
   --header 'Authorization: Basic YWRtaW46cGFzcw==' \
   --data-raw ''```

10) ```curl --location --request GET 'http://localhost:8080/users/list?lastName=astNa&homeLocation=omeLocation' \
    --header 'Authorization: Basic YWRtaW46cGFzcw=='```

11) ```curl --location --request GET 'http://localhost:8080/users/get/username4' \
    --header 'Authorization: Basic YWRtaW46cGFzcw=='```

12) The endpoints are secured with basic auth as required
