package com.weathers.weathers;

import com.weathers.cities.dto.CityDto;
import com.weathers.weathers.dto.CityWeatherDto;
import com.weathers.cities.dto.CitySearchDto;
import com.weathers.cities.entities.City;
import com.weathers.cities.repositories.CityRepository;
import com.weathers.cities.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WeatherService {

    CityRepository cityRepository;
    WeatherApiClient weatherClient;
    CityService cityService;

    @Autowired
    public WeatherService(CityRepository cityRepository, WeatherApiClient weatherClient, CityService cityService) {
        this.cityRepository = cityRepository;
        this.weatherClient = weatherClient;
        this.cityService = cityService;
    }

    public List<CityWeatherDto> listWeatherForCities(CitySearchDto cityLike) {
        List<Long> cities = cityService.listCitiesLike(cityLike).stream().map(City::getId).collect(Collectors.toList());
        if (cities.size() == 0)
            return new ArrayList<>();
        List<CityWeatherDto> weathers = weatherClient.getCitiesWeather(cities);
        weathers.sort(Comparator.comparing(CityDto::getName));
        return weathers;
    }

    public Page<CityWeatherDto> listWeatherForCitiesPaged(CitySearchDto cityLike, Pageable pageable) {
        Page<City> cities = cityService.listCitiesLikePaged(cityLike, pageable);
        List<Long> cityIds = cities.getContent().stream().map(City::getId).collect(Collectors.toList());
        List<CityWeatherDto> weathers = weatherClient.getCitiesWeather(cityIds);
        Page<CityWeatherDto> cityWeatherDtoPage = new PageImpl<>(weathers, cities.getPageable(), cities.getTotalElements());
        return cityWeatherDtoPage;
    }
}
