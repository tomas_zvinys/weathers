package com.weathers.weathers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.weathers.cities.dto.CityDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityWeatherDto extends CityDto {
    private Integer visibility;
    private WindDto wind;
    @JsonProperty("main")
    private MainConditionSetDto mainConditions;
    @JsonProperty("dt")
    private Long timeStamp;

// At this point the openweathermap api seems to have imperfections itself...
// It would be nice to use delegation,
// but since some things (ObjectMapper / ModelMapper) use reflection behind the scenes,
// having a bit of code duplication seems to be a lesser evil than digging through the nuances...
//    @JsonUnwrapped
//    @Delegate
//    CityDto city = new CityDto();
//
//    @JsonUnwrapped
//    @Delegate
//    WeatherDto weatherDto = new WeatherDto();

    public WeatherDto getWeather(){
        return WeatherDto.builder()
                .visibility(getVisibility())
                .wind(getWind())
                .mainConditions(getMainConditions())
                .timeStamp(getTimeStamp())
                .build();
    }

    public CityDto getCity(){
        return CityDto.builder()
                .id(getId())
                .name(getName())
                .country(getCountry())
                .state(getState())
                .coord(getCoord())
                .area(getArea())
                .population(getPopulation())
                .isEnabled(getIsEnabled())
                .timeZone(getTimeZone())
                .build();
    }
}
