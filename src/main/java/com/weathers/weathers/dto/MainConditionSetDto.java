package com.weathers.weathers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MainConditionSetDto {
    Double temp;
    @JsonProperty("feels_like")
    Double feelsLike;
    @JsonProperty("temp_min")
    Double tempMin;
    @JsonProperty("temp_max")
    Double tempMax;
    Integer pressure;
    Integer humidity;
    @JsonProperty("sea_level")
    Integer seaLevel;
    @JsonProperty("grnd_level")
    Integer grndLevel;
}
