package com.weathers.weathers;

import com.weathers.weathers.dto.CityWeatherDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@PropertySource("classpath:secrets.properties")
public class WeatherApiClient {

    private final String apiBaseUrl;
    private final String apiKey;
    private final WebClient webClient;

    @Autowired
    public WeatherApiClient(@Value("${weather.api.key}")String apiKey,
                            @Value("${weather.api.base.url}")String apiBaseUrl)
    {
        this.apiKey = apiKey;
        this.apiBaseUrl = apiBaseUrl;

        this.webClient = WebClient
                .builder()
                .baseUrl(apiBaseUrl)
                .build();
    }

    // I hope it's good enough to parallelize requests to the weather server
    // However it is still a blocking approach and could be done better, by using Mono / Flux everywhere
    // In that case we would have better thread management, utilizing even time gaps between DB and Api calls
    public List<CityWeatherDto> getCitiesWeather(List<Long> cityIds){
        List<CityWeatherDto> responseObjects = Flux.fromIterable(cityIds)
                .flatMap(cityId -> webClient.get()
                        .uri(uriBuilder -> uriBuilder
                                .path("/weather")
                                .queryParam("id", cityId)
                                .queryParam("appid", apiKey)
                                .build())
                        .retrieve()
                        .bodyToMono(CityWeatherDto.class))
                .onErrorResume(WebClientResponseException.class,
                        ex -> ex.getRawStatusCode() == 404 ? Flux.empty() : Mono.error(ex))
                .log()
                .collectList()
                .block();

        return responseObjects;
    }

}
