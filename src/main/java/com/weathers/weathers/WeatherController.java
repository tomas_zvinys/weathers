package com.weathers.weathers;

import com.weathers.weathers.dto.CityWeatherDto;
import com.weathers.cities.dto.CitySearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/weathers")
public class WeatherController {
    WeatherService weatherService;

    @Autowired
    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/search")
    public ResponseEntity<List<CityWeatherDto>> getBasicCityWeather(CitySearchDto cityLike){
        List<CityWeatherDto> citiesWeathers = weatherService.listWeatherForCities(cityLike);
        return new ResponseEntity<>(citiesWeathers, HttpStatus.OK);
    }

    @GetMapping("/search/pages")
    public Page<CityWeatherDto> getBasicCityWeather(
            CitySearchDto cityLike,
            @PageableDefault(size = 5, sort = {"name"}, direction = Sort.Direction.ASC) Pageable pageable)
    {
        return weatherService.listWeatherForCitiesPaged(cityLike, pageable);
    }
}
