package com.weathers.abstracts;

import org.springframework.core.GenericTypeResolver;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractCriteriaDao <T> {
    protected final EntityManager entityManager;
    protected final CriteriaBuilder criteriaBuilder;

    public AbstractCriteriaDao(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    protected List<Order> makeOrderList(Root<T> criteriaRoot, Pageable entityPage){
        return entityPage.getSort().stream()
                .map(s -> makeOrder(s, criteriaRoot))
                .collect(Collectors.toList());
    }

    private Order makeOrder(Sort.Order sort, Root<T> root){
        Expression<T> expression = root.get(sort.getProperty());
        if (sort.isAscending())
            return criteriaBuilder.asc(expression);
        return criteriaBuilder.desc(expression);
    }

    protected long getTotalCount(Predicate predicate) {
        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> countRoot = countQuery.from(getClazz());
        countQuery.select(criteriaBuilder.count(countRoot)).where(predicate);
        return entityManager.createQuery(countQuery).getSingleResult();
    }

    private Class<T> getClazz() {
        return (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), AbstractCriteriaDao.class);
    }
}
