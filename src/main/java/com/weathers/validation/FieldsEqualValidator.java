package com.weathers.validation;

import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

@Component
public class FieldsEqualValidator implements ConstraintValidator<FieldsEqual, Object> {
    private String firstFieldName;
    private String secondFieldName;
    private String message;

    @Override
    public void initialize(FieldsEqual constraintAnnotation) {
        firstFieldName = constraintAnnotation.fieldName1();
        secondFieldName = constraintAnnotation.fieldName2();
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintValidatorContext) {
        try {
            Object first = getFieldByName(obj, firstFieldName);
            Object second = getFieldByName(obj, secondFieldName);

            boolean isValid = first.equals(second);

            if (!isValid) {
                constraintValidatorContext
                        .disableDefaultConstraintViolation();
                constraintValidatorContext
                        .buildConstraintViolationWithTemplate(message)
                        .addPropertyNode(firstFieldName)
                        .addConstraintViolation();
            }
            return isValid;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Object getFieldByName(Object obj, String objFieldName) throws NoSuchFieldException, IllegalAccessException {
        Class<?> clazz = obj.getClass();
        Field declaredField =  clazz.getDeclaredField(objFieldName);
        declaredField.setAccessible(true);

        return declaredField.get(obj);
    }
}
