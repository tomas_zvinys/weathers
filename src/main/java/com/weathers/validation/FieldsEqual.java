package com.weathers.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Validates if values of two (same class) fields are identical
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FieldsEqualValidator.class)
public @interface FieldsEqual {
    String message() default "{fields.mismatch}";
    Class<?> [] groups() default {};
    Class<? extends Payload> [] payload() default {};

    String fieldName1();
    String fieldName2();
}
