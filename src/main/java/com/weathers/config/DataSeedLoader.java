package com.weathers.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weathers.cities.entities.City;
import com.weathers.cities.repositories.CityRepository;
import com.weathers.user.User;
import com.weathers.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Configuration
public class DataSeedLoader implements CommandLineRunner {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final CityRepository cityRepository;
    private final String dataSeedLoad;
    private final DataSource dataSource;

    @Autowired
    public DataSeedLoader(PasswordEncoder passwordEncoder,
                          UserRepository userRepository,
                          CityRepository cityRepository,
                          @Value("${data.seed.load:#{null}}") String dataSeedLoad, DataSource dataSource) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.cityRepository = cityRepository;
        this.dataSeedLoad = dataSeedLoad;
        this.dataSource = dataSource;
    }

    @Override
    public void run(String... args) throws Exception {
        if (dataSeedLoad == null)
            return;

        if ("full".equals(dataSeedLoad))
            loadCityDataSeed();

        if ("full".equals(dataSeedLoad) || "user".equals(dataSeedLoad))
            loadUserDataSeed();
    }

    private void loadUserDataSeed(){
        if (userRepository.existsByUsernameIgnoreCase("admin"))
            return;
        User adminUser = new User();
        adminUser.setUsername("admin");
        adminUser.setPassword(passwordEncoder.encode("pass"));
        adminUser.setRole("ADMIN");
        userRepository.save(adminUser);
    }

    private void loadCityDataSeed(){
        // read json and write to db
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<City>> typeReference = new TypeReference<List<City>>(){};
        InputStream inputStream = TypeReference.class.getResourceAsStream("/dataSeed/city.list.json");
        try {
            List<City> cities = mapper.readValue(inputStream, typeReference);
            cityRepository.saveAll(cities);
            System.out.println("Cities Saved!");
        } catch (IOException e){
            System.out.println("Unable to save cities: " + e.getMessage());
        }
    }
}
