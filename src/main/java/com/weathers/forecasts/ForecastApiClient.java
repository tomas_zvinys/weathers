package com.weathers.forecasts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@PropertySource("classpath:secrets.properties")
public class ForecastApiClient {

    private final String apiBaseUrl;
    private final String apiKey;
    private final WebClient webClient;

    @Autowired
    public ForecastApiClient(@Value("${weather.api.key}")String apiKey,
                             @Value("${weather.api.base.url}")String apiBaseUrl)
    {
        this.apiKey = apiKey;
        this.apiBaseUrl = apiBaseUrl;

        this.webClient = WebClient
                .builder()
                .baseUrl(apiBaseUrl)
                .build();
    }


    /**
     * We could copy/paste entire client (code duplication)
     * We could make an abstract class and then (weather / forecast)ApiClients would extend it
     * but at this point I see no advantage of overcomplicating things.
     *
     * It is likely we'd like to zip() the two requests, returning both - current weather and forecast
     * and that would be more comfortably done in a single class
     * (till it grows too long of course)
     */
    public List<CityForecastDto> getCitiesForecast(List<Long> cityIds){
        List<CityForecastDto> responseObjects = Flux.fromIterable(cityIds)
                .flatMap(cityId -> webClient.get()
                        .uri(uriBuilder -> uriBuilder
                                .path("/forecast")
                                .queryParam("id", cityId)
                                .queryParam("appid", apiKey)
                                .build())
                        .retrieve()
                        .bodyToMono(CityForecastDto.class))
                .onErrorResume(WebClientResponseException.class,
                        ex -> ex.getRawStatusCode() == 404 ? Flux.empty() : Mono.error(ex))
                .log()
                .collectList()
                .block();

        return responseObjects;
    }
}
