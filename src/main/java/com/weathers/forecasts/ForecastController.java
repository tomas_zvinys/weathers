package com.weathers.forecasts;

import com.weathers.cities.dto.CitySearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/forecasts")
public class ForecastController {
    ForecastService forecastService;

    @Autowired
    public ForecastController(ForecastService forecastService) {
        this.forecastService = forecastService;
    }

    @GetMapping("/search/pages")
    public Page<CityForecastDto> getBasicCityWeather(
            CitySearchDto cityLike,
            @PageableDefault(size = 5, sort = {"name"}, direction = Sort.Direction.ASC) Pageable pageable)
    {
        return forecastService.listForecastForCitiesPaged(cityLike, pageable);
    }
}
