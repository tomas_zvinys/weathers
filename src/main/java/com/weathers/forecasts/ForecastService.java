package com.weathers.forecasts;

import com.weathers.cities.dto.CitySearchDto;
import com.weathers.cities.entities.City;
import com.weathers.cities.repositories.CityRepository;
import com.weathers.cities.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ForecastService {

    CityRepository cityRepository;
    ForecastApiClient forecastClient;
    CityService cityService;

    @Autowired
    public ForecastService(CityRepository cityRepository, ForecastApiClient forecastClient, CityService cityService) {
        this.cityRepository = cityRepository;
        this.forecastClient = forecastClient;
        this.cityService = cityService;
    }

    public Page<CityForecastDto> listForecastForCitiesPaged(CitySearchDto cityLike, Pageable pageable) {
        Page<City> cities = cityService.listCitiesLikePaged(cityLike, pageable);
        List<Long> cityIds = cities.getContent().stream().map(City::getId).collect(Collectors.toList());
        List<CityForecastDto> weathers = forecastClient.getCitiesForecast(cityIds);
        Page<CityForecastDto> cityForecastDtoPage = new PageImpl<>(weathers, cities.getPageable(), cities.getTotalElements());
        return cityForecastDtoPage;
    }
}
