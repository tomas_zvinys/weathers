package com.weathers.forecasts;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.weathers.cities.dto.CityDto;
import com.weathers.weathers.dto.WeatherDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityForecastDto {
    CityDto city;
    @JsonProperty("list")
    List<WeatherDto> forecast;
}
