package com.weathers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeathersApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeathersApplication.class, args);
	}

}
