package com.weathers.error.handling;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

@Builder
@AllArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class DefaultErrorDto {
    @JsonProperty("status")
    private HttpStatus status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("errors") //Could be used to pass errors for field validation on front end side
    private final Map<String, Object> errors = new HashMap<>();

    public void addFieldError(String fieldName, String errorMessage){
        errors.put(fieldName, errorMessage);
    }

}
