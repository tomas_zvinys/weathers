package com.weathers.error.handling;

import lombok.NoArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@NoArgsConstructor
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    protected ResponseEntity<Object> handleNoSuchElementException(NoSuchElementException ex) {
        return badRequestErrDto(ex);
    }

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<Object> handleBadRequestException(BadRequestException ex) {
        return badRequestErrDto(ex);
    }

    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<Object> handleRuntimeException(RuntimeException ex) {
        return badRequestErrDto(ex);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        DefaultErrorDto errDto = DefaultErrorDto.builder().status(status).build();
        BindingResult bindingResult = ex.getBindingResult();
        bindingResult.getFieldErrors().forEach(e -> errDto.addFieldError(e.getField(), e.getDefaultMessage()));

        return ResponseEntity.status(status).body(errDto);
    }

    protected ResponseEntity<Object> badRequestErrDto (Exception ex){
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        DefaultErrorDto errDto = DefaultErrorDto.builder()
                .status(badRequest)
                .message(ex.getMessage()).build();

        return ResponseEntity.status(badRequest).body(errDto);
    }


}
