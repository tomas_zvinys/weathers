package com.weathers.user;

import com.weathers.abstracts.AbstractCriteriaDao;
import com.weathers.user.dto.UserProfileDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserCriteriaDao extends AbstractCriteriaDao<User> {

    @Autowired
    public UserCriteriaDao(EntityManager entityManager) {
        super(entityManager);
    }
    
    public Page<User> findUsersLikePaged(UserProfileDto example, Pageable userPage) {
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);

        Root<User> criteriaRoot = criteriaQuery.from(User.class);
        Predicate predicate = makePredicates(example, criteriaRoot);

        criteriaQuery.select(criteriaRoot)
                .where(predicate)
                .orderBy(makeOrderList(criteriaRoot, userPage));

        TypedQuery<User> query = entityManager.createQuery(criteriaQuery)
                .setMaxResults(userPage.getPageSize())
                .setFirstResult(userPage.getPageNumber() * userPage.getPageSize());

        long userCount = getTotalCount(predicate);

        return new PageImpl<>(query.getResultList(), userPage, userCount);
    }

    private Predicate makePredicates(UserProfileDto example, Root<User> criteriaRoot){
        List<Predicate> predicates = new ArrayList<>();

        if (example.getUsername() != null)
            predicates.add(criteriaBuilder.like(criteriaRoot.get(User_.username), "%" + example.getUsername() + "%"));
        if (example.getFirstName() != null)
            predicates.add(criteriaBuilder.like(criteriaRoot.get(User_.firstName), "%" + example.getFirstName() + "%"));
        if (example.getLastName() != null)
            predicates.add(criteriaBuilder.like(criteriaRoot.get(User_.lastName), "%" + example.getLastName() + "%"));
        if (example.getHomeLocation() != null)
            predicates.add(criteriaBuilder.like(criteriaRoot.get(User_.homeLocation), "%" + example.getHomeLocation() + "%"));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

}
