package com.weathers.user;

import com.weathers.user.dto.UserProfileDto;
import com.weathers.user.dto.UserRegistrationDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    /* 7) Create a way to add user */
    @PostMapping("/add")
    public ResponseEntity<UserProfileDto> addUser(@Valid @RequestBody UserRegistrationDto userToAdd){
        UserProfileDto user = mapper.map(userService.addUser(userToAdd), UserProfileDto.class);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    /* 8) Create a way to update user information */
    @PutMapping("/update/profile")
    public ResponseEntity<UserProfileDto> updateUserProfile(
            @AuthenticationPrincipal User userPrincipal,
            @Valid @RequestBody UserRegistrationDto userToEdit)
    {
        User user = userService.updateUserProfile(userPrincipal, userToEdit);
        UserProfileDto userProfileDto = mapper.map(user, UserProfileDto.class);

        return new ResponseEntity<>(userProfileDto, HttpStatus.OK);
    }

    /* 9) Create a way to remove user information */
    @DeleteMapping("/delete/{username}")
    public ResponseEntity<Object> deleteUser(@PathVariable String username){
        userService.deleteUser(username);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /* 10) Create a way to retrieve a list of users.
     * Add options to filter users by first name, last name, favourite cities */
    @GetMapping("/list")
    public Page<UserProfileDto> listUsersLike(
            UserProfileDto userProfileExample,
            @PageableDefault(size = 5, sort = {"username"}, direction = Sort.Direction.ASC) Pageable pageable)
    {
        return userService.listUsersLikePaged(userProfileExample, pageable)
                .map(User::getProfile);
    }

    /* 11) Create a way to retrieve single user */
    @GetMapping("/get/{username}")
    public ResponseEntity<UserProfileDto> getByUsername(@PathVariable String username){
        UserProfileDto user = mapper.map(userService.findByUsername(username), UserProfileDto.class);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }


}
