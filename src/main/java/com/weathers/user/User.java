package com.weathers.user;

import com.weathers.cities.entities.City;
import com.weathers.user.dto.UserProfileDto;
import com.weathers.user.dto.UserRegistrationDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Oracle incompatible
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String role = null;
    private String homeLocation;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_city",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "city_id")
    )
    private Set<City> favouriteCities = new HashSet<>();


    public User(UserRegistrationDto userDto, Set<City> favouriteCities, String encodedPassword) {
        this.username = userDto.getUsername();
        this.firstName = userDto.getFirstName();
        this.lastName = userDto.getLastName();
        this.homeLocation = userDto.getHomeLocation();
        this.favouriteCities = favouriteCities;
        this.password = encodedPassword;
    }

    public UserProfileDto getProfile(){
        UserProfileDto profile = new UserProfileDto();
        profile.setUsername(username);
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setHomeLocation(homeLocation);
        profile.setFavouriteCities(favouriteCities.stream().map(City::toCityDto).collect(Collectors.toSet()));
        return profile;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority auth = new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return "ROLE_" + role;
            }
        };
        authorities.add(auth);
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
