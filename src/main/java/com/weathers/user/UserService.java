package com.weathers.user;

import com.weathers.cities.dto.CityDto;
import com.weathers.cities.entities.City;
import com.weathers.cities.services.CityService;
import com.weathers.user.dto.UserProfileDto;
import com.weathers.user.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserCriteriaDao userCriteriaDao;
    private final CityService cityService;
    private final PasswordEncoder encoder;

    @Autowired
    public UserService(UserRepository userRepository, UserCriteriaDao userCriteriaDao, CityService cityService, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.userCriteriaDao = userCriteriaDao;
        this.cityService = cityService;
        this.encoder = encoder;
    }

    public User addUser(UserRegistrationDto userData) {
        if (userRepository.existsByUsernameIgnoreCase(userData.getUsername()))
            throw new RuntimeException("username already in use");

        List<Long> cityIds = userData.getFavouriteCities().stream().map(CityDto::getId).collect(Collectors.toList());
        Set<City> favouriteCities = new HashSet<>(cityService.findAll(cityIds));
        User newUser = new User(userData, favouriteCities, encoder.encode(userData.getPassword()));
        return userRepository.save(newUser);
    }

    public User updateUserProfile(User user, UserRegistrationDto userProfileDto) {
        user.setFirstName(userProfileDto.getFirstName());
        user.setLastName(userProfileDto.getLastName());
        user.setHomeLocation(userProfileDto.getHomeLocation());

        List<Long> cityIds = userProfileDto.getFavouriteCities().stream()
                .map(CityDto::getId)
                .distinct()
                .collect(Collectors.toList());

        user.setFavouriteCities(new HashSet<>(cityService.findAll(cityIds)));
        return userRepository.save(user);
    }

    @Transactional
    public void deleteUser(String username) {
        userRepository.deleteByUsername(username);
    }

    public Page<User> listUsersLikePaged(UserProfileDto userProfileExample, Pageable pageable) {
        Page<User> matchingUsers = userCriteriaDao.findUsersLikePaged(userProfileExample, pageable);
        return matchingUsers;
    }

    public User findByUsername(String username) {
        return userRepository.findUserByUsername(username)
                .orElseThrow(() -> new RuntimeException("user " + username + " not found"));
    }
}
