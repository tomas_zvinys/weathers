package com.weathers.user.dto;

import com.weathers.cities.dto.CityDto;
import lombok.Data;

import java.util.Set;

@Data
public class UserProfileDto {
    private String username;
    private String firstName;
    private String lastName;
    private String homeLocation;
    private Set<CityDto> favouriteCities;
}
