package com.weathers.user.dto;

import com.weathers.cities.dto.CityDto;
import com.weathers.validation.FieldsEqual;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Set;

@Data
@FieldsEqual(fieldName1 = "passwordRepeat",
        fieldName2 = "password",
        message = "{password.repeat.mismatch}")
public class UserRegistrationDto {
    @Size(min = 4, max = 255)
    private String username;

    @Size(min = 4, max = 255)
    private String password;

    @Size(min = 4, max = 255)
    private String passwordRepeat;

    @Size(min = 4, max = 255)
    private String firstName;

    @Size(min = 4, max = 255)
    private String lastName;

    @Size(min = 4, max = 255)
    private String homeLocation;

    private Set<CityDto> favouriteCities;
}
