package com.weathers.conditions;

import com.weathers.conditions.dto.CityBasicWeatherDto;
import com.weathers.conditions.dto.CityWeatherForecastDto;
import com.weathers.forecasts.CityForecastDto;
import com.weathers.weathers.dto.CityWeatherDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@PropertySource("classpath:secrets.properties")
public class ConditionsApiClient {

    private final String apiBaseUrl;
    private final String apiKey;
    private final WebClient webClient;

    @Autowired
    public ConditionsApiClient(@Value("${weather.api.key}")String apiKey,
                               @Value("${weather.api.base.url}")String apiBaseUrl)
    {
        this.apiKey = apiKey;
        this.apiBaseUrl = apiBaseUrl;

        this.webClient = WebClient
                .builder()
                .baseUrl(apiBaseUrl)
                .build();
    }

    public List<CityForecastDto> getCitiesForecast(List<Long> cityIds){
        List<CityForecastDto> responseObjects = Flux.fromIterable(cityIds)
                .flatMap(cityId -> webClient.get()
                        .uri(uriBuilder -> uriBuilder
                                .path("/forecast")
                                .queryParam("id", cityId)
                                .queryParam("appid", apiKey)
                                .build())
                        .retrieve()
                        .bodyToMono(CityForecastDto.class))
                .onErrorResume(WebClientResponseException.class,
                        ex -> ex.getRawStatusCode() == 404 ? Flux.empty() : Mono.error(ex))
                .log()
                .collectList()
                .block();

        return responseObjects;
    }

    public Mono<CityWeatherForecastDto> getCityWeatherForecast(Long cityId){
        return Mono.zip(getCityWeather(cityId), getCityForecast(cityId))
                .map(tuple -> combineWeatherForecast(tuple.getT1(), tuple.getT2()));
    }

    public Mono<CityForecastDto> getCityForecast(String cityName) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/forecast")
                        .queryParam("q", cityName)
                        .queryParam("appid", apiKey)
                        .build())
                .retrieve()
                .bodyToMono(CityForecastDto.class);
    }

    public Mono<CityForecastDto> getCityForecast(Long cityId) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/forecast")
                        .queryParam("id", cityId)
                        .queryParam("appid", apiKey)
                        .build())
                .retrieve()
                .bodyToMono(CityForecastDto.class);
    }

    public Mono<CityWeatherDto> getCityWeather(Long cityId) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/weather")
                        .queryParam("id", cityId)
                        .queryParam("appid", apiKey)
                        .build())
                .retrieve()
                .bodyToMono(CityWeatherDto.class);
    }

    private CityWeatherForecastDto combineWeatherForecast(CityWeatherDto weather, CityForecastDto forecast){
        CityWeatherForecastDto combo = new CityWeatherForecastDto();
        combo.setCity(forecast.getCity());
        combo.setForecast(forecast.getForecast());
        combo.setWeather(weather.getWeather());
        return combo;
    }

    public List<CityBasicWeatherDto> getCitiesBasicWeather(List<Long> cityIds) {
        return Flux.fromIterable(cityIds)
                .flatMap(cityId -> webClient.get()
                        .uri(uriBuilder -> uriBuilder
                                .path("/weather")
                                .queryParam("id", cityId)
                                .queryParam("appid", apiKey)
                                .build())
                        .retrieve()
                        .bodyToMono(CityWeatherDto.class))
                        .map(cityWeatherDto -> convertToCityBasicWeather(cityWeatherDto))
                .onErrorResume(WebClientResponseException.class,
                        ex -> ex.getRawStatusCode() == 404 ? Flux.empty() : Mono.error(ex))
                .log()
                .collectList()
                .block();
    }

    private CityBasicWeatherDto convertToCityBasicWeather(CityWeatherDto cityWeatherDto) {
        return CityBasicWeatherDto.builder()
                .city(cityWeatherDto.getCity())
                .mainConditions(cityWeatherDto.getMainConditions())
                .build();
    }
}
