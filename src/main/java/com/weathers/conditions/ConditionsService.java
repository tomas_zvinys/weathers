package com.weathers.conditions;

import com.weathers.cities.dto.CityDto;
import com.weathers.cities.dto.CitySearchDto;
import com.weathers.cities.entities.City;
import com.weathers.cities.services.CityService;
import com.weathers.conditions.dto.CityBasicWeatherDto;
import com.weathers.conditions.dto.CityWeatherDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class ConditionsService {

    private final ConditionsApiClient conditionsClient;
    private final CityService cityService;
    private final ModelMapper modelMapper;

    @Autowired
    public ConditionsService(ConditionsApiClient conditionsClient, CityService cityService) {
        this.conditionsClient = conditionsClient;
        this.cityService = cityService;
        modelMapper = new ModelMapper();
    }

    public CityWeatherDto getCityWeatherForecast(String cityName, Boolean withForecast){
        City city = cityService.findCityByName(cityName)
                .orElseThrow(() -> new NoSuchElementException(String.format("City with name %s does not exist", cityName)));
        return getWeatherForecast(withForecast, city);
    }

    public CityWeatherDto getCityWeatherForecast(Double lon, Double lat, Boolean withForecast) {
        City city = cityService.findCityNearby(lon, lat);
        return getWeatherForecast(withForecast, city);
    }

    private CityWeatherDto getWeatherForecast(Boolean withForecast, City city) {
        CityWeatherDto cityConditions;
        if (withForecast != null && withForecast)
            cityConditions = conditionsClient.getCityWeatherForecast(city.getId()).block();
        else
            cityConditions = conditionsClient.getCityWeather(city.getId())
                    .map((weather) -> {
                        CityWeatherDto combo = new CityWeatherDto();
                        combo.setCity(modelMapper.map(city, CityDto.class));
                        combo.setWeather(weather.getWeather());
                        return combo;
                    })
                    .block();
        return cityConditions;
    }

    public Page<CityBasicWeatherDto> pageWeatherForCities(CitySearchDto cityLike, Pageable pageable) {
        Page<City> cities = cityService.listCitiesLikePaged(cityLike, pageable);
        List<Long> cityIds = cities.getContent().stream().map(City::getId).collect(Collectors.toList());
        List<CityBasicWeatherDto> weathers = conditionsClient.getCitiesBasicWeather(cityIds);
        Page<CityBasicWeatherDto> cityBasicWeatherDtoPage = new PageImpl<>(weathers, cities.getPageable(), cities.getTotalElements());
        return cityBasicWeatherDtoPage;
    }
}
