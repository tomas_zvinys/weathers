package com.weathers.conditions;

import com.weathers.cities.dto.CitySearchDto;
import com.weathers.conditions.dto.CityBasicWeatherDto;
import com.weathers.conditions.dto.CityWeatherDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/conditions")
public class ConditionsController {
    private final ConditionsService conditionsService;

    @Autowired
    public ConditionsController(ConditionsService conditionsService) {
        this.conditionsService = conditionsService;
    }

    /* 1) Create a way to fetch a list of cities with basic current weather information
     * (min and max temperature, weather conditions).
     * Add options to filter cities, by their area, population or other information.
     * url example: http://localhost:8080/conditions/basic/search?name=New&country=US */
    @GetMapping("/basic/search")
    public Page<CityBasicWeatherDto> listBasicCityWeather(
            CitySearchDto cityLike,
            @PageableDefault(size = 5, sort = {"name"}, direction = Sort.Direction.ASC) Pageable pageable)
    {
        return conditionsService.pageWeatherForCities(cityLike, pageable);
    }

    /* 2) Create a way to fetch a single city with extended current weather information
     * (temperatures, weather conditions, pressure, humidity, etc).
     * Add options to this request to include a 5 day/3 hour forecast.
     * url example: http://localhost:8080/conditions/Kaunas */
    @GetMapping("/{cityName}")
    public ResponseEntity<CityWeatherDto> getCityWeatherByName(
            @PathVariable String cityName,
            @RequestParam(required = false) Boolean withForecast)
    {
        CityWeatherDto cityConditions = conditionsService.getCityWeatherForecast(cityName, withForecast);
        return new ResponseEntity<>(cityConditions, HttpStatus.OK);
    }

    /* 3) Create a way to fetch current weather forecast for user provided coordinates.
     * Add options to this request to include a 5 day/3 hour forecast.
     * url example: http://localhost:8080/conditions/byCoordinates?lon=23.9&lat=55 */
    @GetMapping("/byCoordinates")
    public ResponseEntity<CityWeatherDto> getCityWeatherByCoordinate(
            @RequestParam Double lon,
            @RequestParam Double lat,
            @RequestParam(required = false) Boolean withForecast)
    {
        CityWeatherDto cityConditions = conditionsService.getCityWeatherForecast(lon, lat, withForecast);
        return new ResponseEntity<>(cityConditions, HttpStatus.OK);
    }
}
