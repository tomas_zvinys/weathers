package com.weathers.conditions.dto;

import com.weathers.weathers.dto.WeatherDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityWeatherForecastDto extends CityWeatherDto{
    private List<WeatherDto> forecast;
}
