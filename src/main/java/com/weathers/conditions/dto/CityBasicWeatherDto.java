package com.weathers.conditions.dto;

import com.weathers.cities.dto.CityDto;
import com.weathers.weathers.dto.MainConditionSetDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CityBasicWeatherDto {
    private CityDto city;
    private MainConditionSetDto mainConditions;
}
