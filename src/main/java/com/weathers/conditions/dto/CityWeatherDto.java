package com.weathers.conditions.dto;

import com.weathers.cities.dto.CityDto;
import com.weathers.weathers.dto.WeatherDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CityWeatherDto {
    private CityDto city;
    private WeatherDto weather;
}
