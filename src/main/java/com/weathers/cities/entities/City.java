package com.weathers.cities.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.weathers.cities.dto.CityDto;
import com.weathers.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
/* Mysql is case-sensitive and hibernate seems to favour lowercase
 * Alternatively could make global config, but not required and no time for that... */
@Table(name = "city")
@Getter
@Setter
public class City {
    @Id
    private Long id;
    private String name;
    private String country;
    private String state;
    @Embedded
    private Coordinate coord;

    private Double area;
    private Integer population;
    private Boolean isEnabled;
    private Integer timeZone;

    @JsonIgnore
    @ManyToMany(mappedBy = "favouriteCities")
    private Set<User> likedByUsers = new HashSet<>();

    public CityDto toCityDto(){
        return CityDto.builder()
                .id(id)
                .name(name)
                .country(country)
                .state(state)
                .coord(coord)
                .area(area)
                .population(population)
                .isEnabled(isEnabled)
                .timeZone(timeZone)
                .build();
    }
}
