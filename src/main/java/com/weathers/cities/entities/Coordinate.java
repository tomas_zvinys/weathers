package com.weathers.cities.entities;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class Coordinate {
    Double lon;
    Double lat;
}
