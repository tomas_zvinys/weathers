package com.weathers.cities.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(City.class)
public class City_ {
    public static volatile SingularAttribute<City, Long> id;
    public static volatile SingularAttribute<City, String> name;
    public static volatile SingularAttribute<City, String> country;
    public static volatile SingularAttribute<City, String> Cityname;
    public static volatile SingularAttribute<City, String> state;
    public static volatile SingularAttribute<City, Double> area;
    public static volatile SingularAttribute<City, Integer> population;
    public static volatile SingularAttribute<City, Boolean> isEnabled;
    public static volatile SingularAttribute<City, Integer> timeZone;
    public static volatile SingularAttribute<City, Coordinate> coord;
}
