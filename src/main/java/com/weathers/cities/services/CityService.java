package com.weathers.cities.services;

import com.weathers.cities.dto.CityDto;
import com.weathers.cities.dto.CitySearchDto;
import com.weathers.cities.entities.City;
import com.weathers.cities.repositories.CityCriteriaDao;
import com.weathers.cities.repositories.CityRepository;
import com.weathers.conditions.ConditionsApiClient;
import com.weathers.forecasts.CityForecastDto;
import com.weathers.error.handling.BadRequestException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class CityService {

    private final CityRepository cityRepository;
    private final CityCriteriaDao cityCriteriaDao;
    private final ConditionsApiClient conditionsApiClient;
    private final ModelMapper mapper = new ModelMapper();
    private final String DIGITS_REGEX = "^\\d{1,19}$";

    @Autowired
    public CityService(CityRepository cityRepository, CityCriteriaDao cityCriteriaDao, ConditionsApiClient conditionsApiClient) {
        this.cityRepository = cityRepository;
        this.cityCriteriaDao = cityCriteriaDao;
        this.conditionsApiClient = conditionsApiClient;
    }

    public List<City> findAll(List<Long> cityIds){
        return cityRepository.findAllById(cityIds);
    }

    public List<City> listCitiesLike(CitySearchDto cityLike) {
        List<City> matchingCities = cityCriteriaDao.findCitiesLike(cityLike);
        return matchingCities;
    }

    public Page<City> listCitiesLikePaged(CitySearchDto cityLike, Pageable pageable) {
        Page<City> matchingCities = cityCriteriaDao.findCitiesLikePaged(cityLike, pageable);
        return matchingCities;
    }

    public Optional<City> findCityByName(String cityName) {
        if (cityName == null)
            return Optional.empty();

        Optional<City> city;
        if (cityName.contains(",")) {
            String[] stringSplit = cityName.split(",");
            city = cityRepository.findCityByNameCountryState(stringSplit[0], stringSplit[1], stringSplit[1]);
        }
        else
            city = cityRepository.findTopByName(cityName);
        return city;
    }

    public City findCityNearby(Double lon, Double lat) {
        City city = cityRepository.findCityNearby(lon, lat);
        return city;
    }

    public City addCity(CityDto cityToAdd) {
        /* Well... I did raise question of what should I do about city fetching...
         * ... so as instructed - doing whatever I want here (add only when name non-existing)) */

        if (cityToAdd == null || cityToAdd.getName() == null)
            /* We could put some @javax.validation.constraints onto CityDto fields,
             * but I'm short of time and that wasn't requested */
            throw new BadRequestException("City name missing");

        String cityName = cityToAdd.getName();
        Optional<City> existingCity = findCityByName(cityName);
        if (existingCity.isPresent())
            throw new BadRequestException("City" + cityName + "already exists");

        CityForecastDto cityData = conditionsApiClient.getCityForecast(cityName).block();
        if (cityData == null || cityData.getCity() == null || cityData.getCity().getId() == null)
            throw new RuntimeException("Failed getting cityId for" + cityName);

        City city = mapper.map(cityToAdd, City.class);
        city.setId(cityData.getCity().getId());
        return cityRepository.save(city);
    }

    public City updateCity(CityDto cityToUpdate) {
        final String cityName = cityToUpdate.getName();
        City existingCity = findCityByName(cityName)
                .orElseThrow(() -> new NoSuchElementException(String.format("City with name %s does not exist", cityName)));

        final long cityId = existingCity.getId();
        existingCity = mapper.map(cityToUpdate, City.class);
        existingCity.setId(cityId);
        existingCity.setName(cityName);
        return cityRepository.save(existingCity);
    }

    public void deleteCity(String citySpecifier) {
        if (citySpecifier.matches(DIGITS_REGEX))
            try {
                cityRepository.findById(Long.valueOf(citySpecifier))
                        .ifPresent(cityRepository::delete);
            } catch (NumberFormatException ignore) {}
        findCityByName(citySpecifier)
                .ifPresent(cityRepository::delete);
    }

    public City updateAvailability(CityDto cityToUpdate) {
        Optional<City> cityOptional = Optional.empty();
        if (cityToUpdate.getId() != null)
            cityOptional = cityRepository.findById(cityToUpdate.getId());

        final String citySpecifier = cityToUpdate.getName();
        if (!cityOptional.isPresent()) {
            cityOptional = findCityByName(citySpecifier);
        }
        City existingCity = cityOptional
                .orElseThrow(() -> new NoSuchElementException(String.format("City with name %s does not exist", citySpecifier)));

        existingCity.setIsEnabled(cityToUpdate.getIsEnabled());
        return cityRepository.save(existingCity);
    }
}
