package com.weathers.cities.repositories;

import com.weathers.cities.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CityRepository  extends JpaRepository<City, Long> /*, JpaSpecificationExecutor<City>*/ {
    /* By extending JpaSpecificationExecutor it is possible to do something like:
     *      cityRepository.findAll(where(hasCountry(country)).and(populationGreaterThan(populationMin)));
     * However, it was not fully clear how to handle case when either of features is null.
     * Thus for more flexibility using custom DAO for the purpose instead.
     */


    /* Theoretically there can be Vilnius in LT and Vilnius in AK (US state Alaska), so... still some ambiguity  */
    @Query( " select c from City c" +
            " where c.name = :name and (c.country = :country or c.state = :state)")
    Optional<City> findCityByNameCountryState2(String name, String country, String state);

    default Optional<City> findCityByNameCountryState(String name, String country, String state){
        Optional<City> city = findTopByNameAndCountry(name, country);
        if (!city.isPresent())
            city = findTopByNameAndState(name, state);
        return city;
    }

    Optional<City> findTopByName(String name);

    Optional<City> findTopByNameAndCountry(String name, String country);

    Optional<City> findTopByNameAndState(String name, String state);

    /* Note:
     * - we could use ArangoDB dependency to get this functionality out of the box (but that's dependent on MySql)
     * - JPQL, seems to be unable to handle "select from select" queries
     * - we can write a native query (but different engines have different syntax to fetch first row only. Nasty!)
     * Anyway... To make it more efficient it would be wise to have indexes on lon and lat.
     */
    @Query(value =
            " select c.*, ((lat - :lat) * (lat - :lat) + (lon - :lon) * (lon - :lon)) dist" +
            " from city c" +
            " order by dist" +
            " limit 1", nativeQuery = true
    )
    City findCityNearby(@Param("lon")Double lon, @Param("lat")Double lat);
}