package com.weathers.cities.repositories;

import com.weathers.abstracts.AbstractCriteriaDao;
import com.weathers.cities.dto.CitySearchDto;
import com.weathers.cities.entities.City;
import com.weathers.cities.entities.City_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class CityCriteriaDao extends AbstractCriteriaDao<City> {
    private final Integer DEFAULT_PAGE_SIZE = 20;

    @Autowired
    public CityCriteriaDao(EntityManager entityManager) {
        super(entityManager);
    }

    public Page<City> findCitiesLikePaged(CitySearchDto example, Pageable cityPage) {
        CriteriaQuery<City> criteriaQuery = criteriaBuilder.createQuery(City.class);

        Root<City> criteriaRoot = criteriaQuery.from(City.class);
        Predicate predicate = makePredicates(example, criteriaRoot);

        criteriaQuery.select(criteriaRoot)
                .where(predicate)
                .orderBy(makeOrderList(criteriaRoot, cityPage));

        TypedQuery<City> query = entityManager.createQuery(criteriaQuery)
                .setMaxResults(cityPage.getPageSize())
                .setFirstResult(cityPage.getPageNumber() * cityPage.getPageSize());

        long cityCount = getTotalCount(predicate);

        return new PageImpl<>(query.getResultList(), cityPage, cityCount);
    }

    public List<City> findCitiesLike(CitySearchDto example) {
        return findCitiesLike(example, DEFAULT_PAGE_SIZE);
    }

    public List<City> findCitiesLike(CitySearchDto example, Integer pageSize) {
        CriteriaQuery<City> criteriaQuery = criteriaBuilder.createQuery(City.class);
        Root<City> criteriaRoot = criteriaQuery.from(City.class);

        Predicate predicate = makePredicates(example, criteriaRoot);
        criteriaQuery.select(criteriaRoot).where(predicate);

        Integer pageNr = (example.getPageNr() != null && example.getPageNr() > 0) ? example.getPageNr() : 0;

        TypedQuery<City> query = entityManager.createQuery(criteriaQuery)
                .setMaxResults(pageSize)
                .setFirstResult(pageNr * pageSize);
        return query.getResultList();
    }

    private Predicate makePredicates(CitySearchDto example, Root<City> criteriaRoot){
        List<Predicate> predicates = new ArrayList<>();

        if (example.getName() != null)
            predicates.add(criteriaBuilder.like(criteriaRoot.get(City_.name), "%" + example.getName() + "%"));
        if (example.getCountry() != null)
            predicates.add(criteriaBuilder.like(criteriaRoot.get(City_.country), "%" + example.getCountry() + "%"));
        if (example.getState() != null)
            predicates.add(criteriaBuilder.like(criteriaRoot.get(City_.state), "%" + example.getState() + "%"));

        if (example.getIsEnabled() != null)
            predicates.add(criteriaBuilder.equal(criteriaRoot.get(City_.isEnabled), example.getIsEnabled()));

        if (example.getPopulationMin() != null)
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(criteriaRoot.get(City_.population), example.getPopulationMin()));
        if (example.getPopulationMax() != null)
            predicates.add(criteriaBuilder.lessThanOrEqualTo(criteriaRoot.get(City_.population), example.getPopulationMax()));

        if (example.getAreaMin() != null)
            predicates.add(criteriaBuilder.greaterThanOrEqualTo(criteriaRoot.get(City_.area), example.getAreaMin()));
        if (example.getAreaMax() != null)
            predicates.add(criteriaBuilder.lessThanOrEqualTo(criteriaRoot.get(City_.area), example.getAreaMax()));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

}
