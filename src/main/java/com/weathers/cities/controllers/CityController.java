package com.weathers.cities.controllers;

import com.weathers.cities.dto.CityDto;
import com.weathers.cities.dto.CitySearchDto;
import com.weathers.cities.services.CityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/cities")
public class CityController {
    private final CityService cityService;
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping("/search")
    public ResponseEntity<List<CityDto>> listCities(CitySearchDto cityLike){
        List<CityDto> cities = cityService.listCitiesLike(cityLike).stream()
                .map(city -> mapper.map(city, CityDto.class))
                .collect(Collectors.toList());
        return new ResponseEntity<>(cities, HttpStatus.OK);
    }

    @GetMapping("/search/pages")
    public Page<CityDto> listCities(
            CitySearchDto cityLike,
            @PageableDefault(size = 5, sort = {"name"}, direction = Sort.Direction.ASC) Pageable pageable)
    {
        return cityService.listCitiesLikePaged(cityLike, pageable)
                .map(city -> mapper.map(city, CityDto.class));
    }

    /* 4) Create a way to add a city to the list of available cities */
    @PostMapping("/add")
    public ResponseEntity<CityDto> addCity(@RequestBody CityDto cityToAdd){
        CityDto city = mapper.map(cityService.addCity(cityToAdd), CityDto.class);
        return new ResponseEntity<>(city, HttpStatus.CREATED);
    }

    /* 6) Create a way to update a city information */
    @PutMapping("/update")
    public ResponseEntity<CityDto> updateCity(@RequestBody CityDto cityToUpdate){
        CityDto city = mapper.map(cityService.updateCity(cityToUpdate), CityDto.class);
        return new ResponseEntity<>(city, HttpStatus.OK);
    }

    /* 5) Create a way to remove a city from the list of available cities
    * -- I'm not fully sure what you mean, so here are both delete and disable */
    @DeleteMapping("/delete/{citySpecifier}")
    public ResponseEntity<Object> deleteCity(@PathVariable String citySpecifier){
        cityService.deleteCity(citySpecifier);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/availability")
    public ResponseEntity<CityDto> updateCityAvailability(@RequestBody CityDto cityToUpdate){
        CityDto city = mapper.map(cityService.updateAvailability(cityToUpdate), CityDto.class);
        return new ResponseEntity<>(city, HttpStatus.OK);
    }

}
