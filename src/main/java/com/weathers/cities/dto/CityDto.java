package com.weathers.cities.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.weathers.cities.entities.Coordinate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CityDto {

    Long id;
    String name;
    String country;
    String state;
    Coordinate coord;

    Double area;
    Integer population;
    Boolean isEnabled;
    @JsonProperty("timezone")
    Integer timeZone;

}
