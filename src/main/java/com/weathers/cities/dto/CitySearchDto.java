package com.weathers.cities.dto;

import lombok.Data;

@Data
public class CitySearchDto extends CityDto {
    Integer populationMin;
    Integer populationMax;
    Double areaMin;
    Double areaMax;
    Integer pageNr;
}
