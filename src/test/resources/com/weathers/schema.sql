
drop table if exists city;
create table city
(
    id         bigint       not null
        primary key,
    area       double       null,
    lat        double       null,
    lon        double       null,
    country    varchar(255) null,
    is_enabled bit          null,
    name       varchar(255) null,
    population int          null,
    state      varchar(255) null,
    time_zone  int          null
);

drop table if exists user;
create table user
(
    id            bigint auto_increment
        primary key,
    first_name    varchar(255) null,
    home_location varchar(255) null,
    last_name     varchar(255) null,
    password      varchar(255) null,
    username      varchar(255) null
);

drop table if exists user_city;
create table user_city
(
    user_id bigint not null,
    city_id bigint not null,
    primary key (user_id, city_id)
);