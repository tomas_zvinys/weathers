TRUNCATE TABLE CITY;

INSERT INTO CITY (ID, NAME, STATE, COUNTRY, LON, LAT, AREA, POPULATION, IS_ENABLED)
VALUES (593116, 'Vilnius', '', 'LT', 25.2798, 54.689159, 401,2,true);

INSERT INTO CITY (ID, NAME, STATE, COUNTRY, LON, LAT, AREA, POPULATION, IS_ENABLED)
VALUES (593144, 'Vilkija', '', 'LT', 23.58333, 55.049999, null,4,true);

INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (6618486,'Dainava (Kaunas)','LT',NULL,23.968309,54.915249,NULL,15,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (2892102,'Kau','DE',NULL,7.35,50.616669,NULL,3,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (2892101,'Kaub','DE',NULL,7.76667,50.083328,NULL,1,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (2892093,'Kauerheim','DE',NULL,11.55169,49.420582,NULL,17,NULL);

INSERT INTO CITY (ID, NAME, STATE, COUNTRY, LON, LAT, AREA, POPULATION, IS_ENABLED)
VALUES (593153, 'Vilkaviskis', '', 'LT', 23.032221, 54.651669, null,18,true);

INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (6550500,'Kauern','DE',NULL,12.0833,50.716702,NULL,6,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (2892083,'Kauernhofen','DE',NULL,11.08333,49.76667,NULL,7,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (2892082,'Kaufbach','DE',NULL,13.56667,51.049999,NULL,8,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (2892080,'Kaufbeuren','DE',NULL,10.61667,47.883331,NULL,12,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (6556259,'Kaufering','DE',NULL,10.8595,48.0867,NULL,9,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (4702728,'Kaufman','US','TX',-96.308868,32.58902,NULL,13,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (3336895,'Kaufungen','DE',NULL,9.61861,51.281109,NULL,10,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (653628,'Kauhajoki','FI',NULL,22.25,62.33333,NULL,5,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (7406835,'Kauhan','ID',NULL,114.971802,-8.2673,NULL,11,NULL);
INSERT INTO CITY(id,name,country,state,lon,lat,area,population,is_enabled) VALUES (653617,'Kauhava','FI',NULL,23.01084,63.19419,NULL,16,NULL);

INSERT INTO CITY (ID, NAME, STATE, COUNTRY, LON, LAT, AREA, POPULATION, IS_ENABLED)
VALUES (5128638, 'New York', 'NY', 'US', -75.499901, 43.000351, 122359,14,true);

