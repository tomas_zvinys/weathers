package com.weathers.weathers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weathers.weathers.dto.CityWeatherDto;
import com.weathers.weathers.dto.MainConditionSetDto;
import com.weathers.weathers.dto.WindDto;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class WeatherControllerIntegrationTest {

    private MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static ClientAndServer mockServer;

    @BeforeAll
    public static void startServer() {
        mockServer = startClientAndServer(1080);
    }

    @AfterAll
    public static void stopServer() {
        mockServer.stop();
    }

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/weathers/WeathersControllerIntegrationTest.sql"})
    void findsCitiesWeathersWhenPartOfCityNameProvided() throws Exception {
        mockServer.when(requestSample(593144), exactly(1))
                .respond(response()
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withStatusCode(HttpStatus.OK.value())
                        .withBody(mapper.writeValueAsString(vilkija()))
                );
        mockServer.when(requestSample(593153), exactly(1))
                .respond(response()
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withStatusCode(HttpStatus.OK.value())
                        .withBody(mapper.writeValueAsString(vilkaviskis())));

        this.mockMvc.perform(get("/weathers/search?name=Vilk"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(vilkaviskis().getId()))
                .andExpect(jsonPath("$[0].name").value(vilkaviskis().getName()))
                .andExpect(jsonPath("$[0].country").value(vilkaviskis().getCountry()))
                .andExpect(jsonPath("$[0].wind.speed").value(vilkaviskis().getWind().getSpeed()))
                .andExpect(jsonPath("$[0].wind.deg").value(vilkaviskis().getWind().getDeg()))
                .andExpect(jsonPath("$[0].wind.gust").value(vilkaviskis().getWind().getGust()))
                .andExpect(jsonPath("$[0].main.temp").value(vilkaviskis().getMainConditions().getTemp()))
                .andExpect(jsonPath("$[0].main.feels_like").value(vilkaviskis().getMainConditions().getFeelsLike()))
                .andExpect(jsonPath("$[0].main.temp_min").value(vilkaviskis().getMainConditions().getTempMin()))
                .andExpect(jsonPath("$[0].main.temp_max").value(vilkaviskis().getMainConditions().getTempMax()))
                .andExpect(jsonPath("$[0].main.pressure").value(vilkaviskis().getMainConditions().getPressure()))
                .andExpect(jsonPath("$[0].main.humidity").value(vilkaviskis().getMainConditions().getHumidity()))
                .andExpect(jsonPath("$[0].main.sea_level").value(vilkaviskis().getMainConditions().getSeaLevel()))
                .andExpect(jsonPath("$[0].main.grnd_level").value(vilkaviskis().getMainConditions().getGrndLevel()))

                .andExpect(jsonPath("$[1].id").value(vilkija().getId()))
                .andExpect(jsonPath("$[1].name").value(vilkija().getName()))
                .andExpect(jsonPath("$[1].country").value(vilkija().getCountry()))
                .andExpect(jsonPath("$[1].wind.speed").value(vilkija().getWind().getSpeed()))
                .andExpect(jsonPath("$[1].wind.deg").value(vilkija().getWind().getDeg()))
                .andExpect(jsonPath("$[1].wind.gust").value(vilkija().getWind().getGust()))
                .andExpect(jsonPath("$[1].main.temp").value(vilkija().getMainConditions().getTemp()))
                .andExpect(jsonPath("$[1].main.feels_like").value(vilkija().getMainConditions().getFeelsLike()))
                .andExpect(jsonPath("$[1].main.temp_min").value(vilkija().getMainConditions().getTempMin()))
                .andExpect(jsonPath("$[1].main.temp_max").value(vilkija().getMainConditions().getTempMax()))
                .andExpect(jsonPath("$[1].main.pressure").value(vilkija().getMainConditions().getPressure()))
                .andExpect(jsonPath("$[1].main.humidity").value(vilkija().getMainConditions().getHumidity()))
                .andExpect(jsonPath("$[1].main.sea_level").value(vilkija().getMainConditions().getSeaLevel()))
                .andExpect(jsonPath("$[1].main.grnd_level").value(vilkija().getMainConditions().getGrndLevel()))
        ;
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/weathers/WeathersControllerIntegrationTest.sql"})
    void doesNotFailWhenGets404FromApi() throws Exception {
        mockServer.when(requestSample(593144), exactly(1))
                .respond(response()
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withStatusCode(HttpStatus.OK.value())
                        .withBody(mapper.writeValueAsString(vilkija()))
                );
        mockServer.when(requestSample(593153), exactly(1))
                .respond(response()
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withStatusCode(HttpStatus.NOT_FOUND.value()));

        this.mockMvc.perform(get("/weathers/search?name=Vilk"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(vilkija().getId()))
                .andExpect(jsonPath("$[0].name").value(vilkija().getName()))
                .andExpect(jsonPath("$[0].country").value(vilkija().getCountry()))
                .andExpect(jsonPath("$[0].wind.speed").value(vilkija().getWind().getSpeed()))
                .andExpect(jsonPath("$[0].wind.deg").value(vilkija().getWind().getDeg()))
                .andExpect(jsonPath("$[0].wind.gust").value(vilkija().getWind().getGust()))
                .andExpect(jsonPath("$[0].main.temp").value(vilkija().getMainConditions().getTemp()))
                .andExpect(jsonPath("$[0].main.feels_like").value(vilkija().getMainConditions().getFeelsLike()))
                .andExpect(jsonPath("$[0].main.temp_min").value(vilkija().getMainConditions().getTempMin()))
                .andExpect(jsonPath("$[0].main.temp_max").value(vilkija().getMainConditions().getTempMax()))
                .andExpect(jsonPath("$[0].main.pressure").value(vilkija().getMainConditions().getPressure()))
                .andExpect(jsonPath("$[0].main.humidity").value(vilkija().getMainConditions().getHumidity()))
                .andExpect(jsonPath("$[0].main.sea_level").value(vilkija().getMainConditions().getSeaLevel()))
                .andExpect(jsonPath("$[0].main.grnd_level").value(vilkija().getMainConditions().getGrndLevel()))
        ;
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/weathers/WeathersControllerIntegrationTest.sql"})
    void findsCitiesWeathersWhenPageConditionsProvided() throws Exception {
        mockServer.when(requestSample(593144), exactly(1))
                .respond(response()
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withStatusCode(HttpStatus.OK.value())
                        .withBody(mapper.writeValueAsString(vilkija()))
                );

        this.mockMvc.perform(get("/weathers/search/pages?name=Vilk&country=LT")
                                .param("size", "1")
                                .param("page", "0")
                                .param("sort", "population,asc"))
                .andDo(print())
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.content[0].id").value(vilkija().getId()))
                .andExpect(jsonPath("$.content[0].name").value(vilkija().getName()))
                .andExpect(jsonPath("$.content[0].country").value(vilkija().getCountry()))
                .andExpect(jsonPath("$.content[0].wind.speed").value(vilkija().getWind().getSpeed()))
                .andExpect(jsonPath("$.content[0].wind.deg").value(vilkija().getWind().getDeg()))
                .andExpect(jsonPath("$.content[0].wind.gust").value(vilkija().getWind().getGust()))
                .andExpect(jsonPath("$.content[0].main.temp").value(vilkija().getMainConditions().getTemp()))
                .andExpect(jsonPath("$.content[0].main.feels_like").value(vilkija().getMainConditions().getFeelsLike()))
                .andExpect(jsonPath("$.content[0].main.temp_min").value(vilkija().getMainConditions().getTempMin()))
                .andExpect(jsonPath("$.content[0].main.temp_max").value(vilkija().getMainConditions().getTempMax()))
                .andExpect(jsonPath("$.content[0].main.pressure").value(vilkija().getMainConditions().getPressure()))
                .andExpect(jsonPath("$.content[0].main.humidity").value(vilkija().getMainConditions().getHumidity()))
                .andExpect(jsonPath("$.content[0].main.sea_level").value(vilkija().getMainConditions().getSeaLevel()))
                .andExpect(jsonPath("$.content[0].main.grnd_level").value(vilkija().getMainConditions().getGrndLevel()))

                .andExpect(jsonPath("$.totalPages").value(2))
                .andExpect(jsonPath("$.size").value(1))
                .andExpect(jsonPath("$.first").value(true))
                .andExpect(jsonPath("$.last").value(false))
                .andExpect(jsonPath("$.sort.sorted").value(true))
        ;
    }

    private HttpRequest requestSample(Integer cityId){
        return request()
                .withMethod("GET")
                .withPath("/weather")
                .withQueryStringParameter("id", String.valueOf(cityId))
                .withQueryStringParameter("appId", "registeredUserApiKey");
    }

    private CityWeatherDto vilkija(){
        CityWeatherDto vilkija = new CityWeatherDto();
        vilkija.setId(593144L);
        vilkija.setName("Vilkija");
        vilkija.setWind(new WindDto(2.23, 162, 4.48));
        vilkija.setMainConditions(new MainConditionSetDto(265.11, 260.98, 265.11, 265.16, 1021, 96, 1021, 1011));
        return vilkija;
    }

    private CityWeatherDto vilkaviskis(){
        CityWeatherDto vilkija = new CityWeatherDto();
        vilkija.setId(593153L);
        vilkija.setName("Vilkaviškis");
        vilkija.setWind(new WindDto(2.89, 143, 6.72));
        vilkija.setMainConditions(new MainConditionSetDto(265.39, 260.45, 265.39, 265.40, 1020, 94, 1020, 1013));
        return vilkija;
    }

}