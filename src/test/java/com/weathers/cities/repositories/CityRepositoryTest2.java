package com.weathers.cities.repositories;

import com.weathers.cities.entities.City;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest//(properties = {"data.seed.load=true"})
@ExtendWith(SpringExtension.class)
class CityRepositoryTest2 {

    @Autowired
    CityRepository cityRepository;

    @Test
    void repositoryWired() {
        assertNotNull(cityRepository);
    }

    @Test
    @Sql("/com/weathers/schema.sql")
    void dataSeedNotLoadedWhenPropertyMissing() {
        List<City> cities = cityRepository.findAll();
        assertNotNull(cities);
        assertEquals(0, cities.size());
    }
}