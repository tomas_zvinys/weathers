package com.weathers.cities.repositories;

import com.weathers.cities.entities.City;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Disabled(value = "Looks like CommandLineRunner runs sooner than tables get created in H2")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@SpringBootTest(properties = {"hibernate.ddl-auto=create-drop", "data.seed.load=full"})
@ExtendWith(SpringExtension.class)
class CityRepositoryTest {

    @Autowired
    CityRepository cityRepository;

    @Test
    void repositoryWired() {
        assertNotNull(cityRepository);
    }

    @Test
    void dataSeedLoaded() {
        List<City> cities = cityRepository.findAll();
        assertNotNull(cities);
        assertEquals(4, cities.size());

        City cityVilnius = cityRepository.findById(593116L).orElse(null);
        assertNotNull(cityVilnius);
        assertEquals("Vilnius", cityVilnius.getName());
        assertEquals("", cityVilnius.getState());
        assertEquals("LT", cityVilnius.getCountry());
        assertNotNull(cityVilnius.getCoord());
        assertEquals(Double.valueOf(25.2798), cityVilnius.getCoord().getLon());
        assertEquals(Double.valueOf(54.689159), cityVilnius.getCoord().getLat());
    }
}