package com.weathers.cities.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@ExtendWith(SpringExtension.class)
class CityControllerIntegrationTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesWithPartOfName() throws Exception {
        this.mockMvc.perform(get("/cities/search?name=ilnius"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(593116))
                .andExpect(jsonPath("$[0].name").value("Vilnius"))
                .andExpect(jsonPath("$[0].country").value("LT"))
                .andExpect(jsonPath("$[0].state").value(""))
                .andExpect(jsonPath("$[0].coord.lon").value(25.2798))
                .andExpect(jsonPath("$[0].coord.lat").value(54.689159))
                .andExpect(jsonPath("$[0].area").value(401))
                .andExpect(jsonPath("$[0].population").value(706832))
                .andExpect(jsonPath("$[0].isEnabled").value(true));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesByCountry() throws Exception {
        this.mockMvc.perform(get("/cities/search?country=LT"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(3))
                .andExpect(jsonPath("$[0].id").value(593116))
                .andExpect(jsonPath("$[0].name").value("Vilnius"))
                .andExpect(jsonPath("$[0].country").value("LT"))
                .andExpect(jsonPath("$[1].id").value(593144))
                .andExpect(jsonPath("$[1].name").value("Vilkija"))
                .andExpect(jsonPath("$[1].country").value("LT"))
                .andExpect(jsonPath("$[2].id").value(593153))
                .andExpect(jsonPath("$[2].name").value("Vilkaviskis"))
                .andExpect(jsonPath("$[2].country").value("LT"));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesByState() throws Exception {
        this.mockMvc.perform(get("/cities/search?state=NY"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].id").value(5128638))
                .andExpect(jsonPath("$[0].name").value("New York"))
                .andExpect(jsonPath("$[0].country").value("US"))
                .andExpect(jsonPath("$[0].state").value("NY"));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesByMinArea() throws Exception {
        this.mockMvc.perform(get("/cities/search?areaMin=1000"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].id").value(5128638))
                .andExpect(jsonPath("$[0].name").value("New York"))
                .andExpect(jsonPath("$[0].country").value("US"))
                .andExpect(jsonPath("$[0].state").value("NY"))
                .andExpect(jsonPath("$[0].area").value(122359));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesByMaxArea() throws Exception {
        this.mockMvc.perform(get("/cities/search?areaMax=1000"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].id").value(593116))
                .andExpect(jsonPath("$[0].name").value("Vilnius"))
                .andExpect(jsonPath("$[0].country").value("LT"))
                .andExpect(jsonPath("$[0].area").value(401));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesByMinPopulation() throws Exception {
        this.mockMvc.perform(get("/cities/search?populationMin=1000000"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].id").value(5128638))
                .andExpect(jsonPath("$[0].name").value("New York"))
                .andExpect(jsonPath("$[0].country").value("US"))
                .andExpect(jsonPath("$[0].state").value("NY"))
                .andExpect(jsonPath("$[0].population").value(8804190));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesByMaxPopulation() throws Exception {
        this.mockMvc.perform(get("/cities/search?populationMax=10000"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(2))
                .andExpect(jsonPath("$[0].id").value(593144))
                .andExpect(jsonPath("$[0].name").value("Vilkija"))
                .andExpect(jsonPath("$[0].country").value("LT"))
                .andExpect(jsonPath("$[0].population").value(2326))
                .andExpect(jsonPath("$[1].id").value(593153))
                .andExpect(jsonPath("$[1].name").value("Vilkaviskis"))
                .andExpect(jsonPath("$[1].country").value("LT"))
                .andExpect(jsonPath("$[1].population").value(9444));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerIntegrationTest.sql"})
    void findsCitiesByMultipleFeatures() throws Exception {
        this.mockMvc.perform(get("/cities/search?populationMin=10000&areaMax=500"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[0].id").value(593116))
                .andExpect(jsonPath("$[0].name").value("Vilnius"))
                .andExpect(jsonPath("$[0].country").value("LT"))
                .andExpect(jsonPath("$[0].population").value(706832))
                .andExpect(jsonPath("$[0].area").value(401));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerSortOrderTest.sql"})
    void ordersAndSortsByFeatures() throws Exception {
        this.mockMvc.perform(
                        get("/cities/search/pages?name=Kau")
                                .param("size", "3")
                                .param("page", "0")
                                .param("sort", "country,desc")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(3))
                .andExpect(jsonPath("$.content[0].id").value(4702728))
                .andExpect(jsonPath("$.content[0].name").value("Kaufman"))
                .andExpect(jsonPath("$.content[0].country").value("US"))
                .andExpect(jsonPath("$.content[0].coord.lon").value(-96.308868))
                .andExpect(jsonPath("$.content[0].coord.lat").value(32.58902))

                .andExpect(jsonPath("$.content[1].id").value(6618486))
                .andExpect(jsonPath("$.content[1].name").value("Dainava (Kaunas)"))
                .andExpect(jsonPath("$.content[1].country").value("LT"))
                .andExpect(jsonPath("$.content[1].coord.lon").value(23.968309))
                .andExpect(jsonPath("$.content[1].coord.lat").value(54.915249))

                .andExpect(jsonPath("$.content[2].id").value(7406835))
                .andExpect(jsonPath("$.content[2].name").value("Kauhan"))
                .andExpect(jsonPath("$.content[2].country").value("ID"))
                .andExpect(jsonPath("$.content[2].coord.lon").value(114.971802))
                .andExpect(jsonPath("$.content[2].coord.lat").value(-8.2673))

                .andExpect(jsonPath("$.size").value(3))
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.empty").value(false))
                .andExpect(jsonPath("$.first").value(true))
                .andExpect(jsonPath("$.last").value(false))
                .andExpect(jsonPath("$.totalPages").value(5))
                .andExpect(jsonPath("$.totalElements").value(14));
    }

    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/cities/controllers/CityControllerSortOrderTest.sql"})
    void ordersAndSortsByFeaturesAndReturnsSecondPage() throws Exception {
        this.mockMvc.perform(
                        get("/cities/search/pages")
                                .param("size", "5")
                                .param("page", "1")
                                .param("sort", "population,asc")
                )
                .andDo(print())
                .andExpect(status().isOk())
                //.andExpect(jsonPath("$.content.length()").value(5))
                .andExpect(jsonPath("$.content[0].id").value(6550500))
                .andExpect(jsonPath("$.content[0].name").value("Kauern"))
                .andExpect(jsonPath("$.content[0].population").value(6))

                .andExpect(jsonPath("$.content[1].id").value(2892083))
                .andExpect(jsonPath("$.content[1].name").value("Kauernhofen"))
                .andExpect(jsonPath("$.content[1].population").value(7))

                .andExpect(jsonPath("$.content[2].id").value(2892082))
                .andExpect(jsonPath("$.content[2].name").value("Kaufbach"))
                .andExpect(jsonPath("$.content[2].population").value(8))

                .andExpect(jsonPath("$.content[3].id").value(6556259))
                .andExpect(jsonPath("$.content[3].name").value("Kaufering"))
                .andExpect(jsonPath("$.content[3].population").value(9))

                .andExpect(jsonPath("$.content[4].id").value(3336895))
                .andExpect(jsonPath("$.content[4].name").value("Kaufungen"))
                .andExpect(jsonPath("$.content[4].population").value(10))

                .andExpect(jsonPath("$.size").value(5))
                .andExpect(jsonPath("$.number").value(1))
                .andExpect(jsonPath("$.empty").value(false))
                .andExpect(jsonPath("$.first").value(false))
                .andExpect(jsonPath("$.last").value(false))
                .andExpect(jsonPath("$.totalPages").value(4))
                .andExpect(jsonPath("$.totalElements").value(18));
    }
}