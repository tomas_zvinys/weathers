package com.weathers.forecasts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weathers.cities.dto.CityDto;
import com.weathers.weathers.dto.MainConditionSetDto;
import com.weathers.weathers.dto.WeatherDto;
import com.weathers.weathers.dto.WindDto;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ForecastControllerIntegrationTest {

    private MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static ClientAndServer mockServer;

    @BeforeAll
    public static void startServer() {
        mockServer = startClientAndServer(1080);
    }

    @AfterAll
    public static void stopServer() {
        mockServer.stop();
    }

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
    @Test
    @Sql({"/com/weathers/schema.sql", "/com/weathers/weathers/WeathersControllerIntegrationTest.sql"})
    void findsCitiesForecastsWhenPageConditionsProvidedAndIgnores404() throws Exception {
        mockServer.when(requestSample(593144), exactly(1))
                .respond(response()
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withStatusCode(HttpStatus.OK.value())
                        .withBody(mapper.writeValueAsString(vilkija()))
                );
        mockServer.when(requestSample(593153), exactly(1))
                .respond(response()
                        .withHeader("Content-Type", "application/json; charset=utf-8")
                        .withStatusCode(HttpStatus.NOT_FOUND.value()));

        CityDto city = vilkija().getCity();
        WeatherDto weather1 = vilkija().getForecast().get(0);
        WeatherDto weather2 = vilkija().getForecast().get(1);

        this.mockMvc.perform(get("/forecasts/search/pages?name=Vilk&country=LT")
                        .param("size", "2")
                        .param("page", "0")
                        .param("sort", "population,asc"))
                .andDo(print())
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.content", hasSize(1)))
                .andExpect(jsonPath("$.content[0].city.id").value(city.getId()))
                .andExpect(jsonPath("$.content[0].city.name").value(city.getName()))
                .andExpect(jsonPath("$.content[0].city.country").value(city.getCountry()))

                .andExpect(jsonPath("$.content[0].list", hasSize(2)))
                .andExpect(jsonPath("$.content[0].list[0].wind.speed").value(weather1.getWind().getSpeed()))
                .andExpect(jsonPath("$.content[0].list[0].wind.deg").value(weather1.getWind().getDeg()))
                .andExpect(jsonPath("$.content[0].list[0].wind.gust").value(weather1.getWind().getGust()))

                .andExpect(jsonPath("$.content[0].list[0].main.temp").value(weather1.getMainConditions().getTemp()))
                .andExpect(jsonPath("$.content[0].list[0].main.feels_like").value(weather1.getMainConditions().getFeelsLike()))
                .andExpect(jsonPath("$.content[0].list[0].main.temp_min").value(weather1.getMainConditions().getTempMin()))
                .andExpect(jsonPath("$.content[0].list[0].main.temp_max").value(weather1.getMainConditions().getTempMax()))
                .andExpect(jsonPath("$.content[0].list[0].main.pressure").value(weather1.getMainConditions().getPressure()))
                .andExpect(jsonPath("$.content[0].list[0].main.humidity").value(weather1.getMainConditions().getHumidity()))
                .andExpect(jsonPath("$.content[0].list[0].main.sea_level").value(weather1.getMainConditions().getSeaLevel()))
                .andExpect(jsonPath("$.content[0].list[0].main.grnd_level").value(weather1.getMainConditions().getGrndLevel()))

                .andExpect(jsonPath("$.content[0].list[0].visibility").value(weather1.getVisibility()))

                .andExpect(jsonPath("$.content[0].list[1].wind.speed").value(weather2.getWind().getSpeed()))
                .andExpect(jsonPath("$.content[0].list[1].wind.deg").value(weather2.getWind().getDeg()))
                .andExpect(jsonPath("$.content[0].list[1].wind.gust").value(weather2.getWind().getGust()))

                .andExpect(jsonPath("$.content[0].list[1].main.temp").value(weather2.getMainConditions().getTemp()))
                .andExpect(jsonPath("$.content[0].list[1].main.feels_like").value(weather2.getMainConditions().getFeelsLike()))
                .andExpect(jsonPath("$.content[0].list[1].main.temp_min").value(weather2.getMainConditions().getTempMin()))
                .andExpect(jsonPath("$.content[0].list[1].main.temp_max").value(weather2.getMainConditions().getTempMax()))
                .andExpect(jsonPath("$.content[0].list[1].main.pressure").value(weather2.getMainConditions().getPressure()))
                .andExpect(jsonPath("$.content[0].list[1].main.humidity").value(weather2.getMainConditions().getHumidity()))
                .andExpect(jsonPath("$.content[0].list[1].main.sea_level").value(weather2.getMainConditions().getSeaLevel()))
                .andExpect(jsonPath("$.content[0].list[1].main.grnd_level").value(weather2.getMainConditions().getGrndLevel()))

                .andExpect(jsonPath("$.content[0].list[1].visibility").value(weather2.getVisibility()))

                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.size").value(2))
                .andExpect(jsonPath("$.first").value(true))
                .andExpect(jsonPath("$.last").value(true))
                .andExpect(jsonPath("$.sort.sorted").value(true))
        ;
    }


    private CityForecastDto vilkija(){
        CityForecastDto vilkija = new CityForecastDto();
        vilkija.setCity(
                CityDto.builder()
                        .id(593144L)
                        .name("Vilkija")
                        .build()
        );

        List<WeatherDto> forecast = new ArrayList<WeatherDto>(){{
            add(WeatherDto.builder()
                    .wind(new WindDto(0.01, 1, 0.1))
                    .mainConditions(new MainConditionSetDto(265.1, 260.1, 265.1, 265.1, 1021, 11, 1021, 1011))
                    .visibility(100)
                    .timeStamp(1641243600L)
                    .build());

            add(WeatherDto.builder()
                    .wind(new WindDto(0.02, 2, 0.2))
                    .mainConditions(new MainConditionSetDto(265.2, 260.2, 265.2, 265.2, 1022, 12, 1022, 1012))
                    .visibility(200)
                    .timeStamp(1641254400L)
                    .build());
        }};
        vilkija.setForecast(forecast);
        return vilkija;
    }


    private HttpRequest requestSample(Integer cityId){
        return request()
                .withMethod("GET")
                .withPath("/forecast")
                .withQueryStringParameter("id", String.valueOf(cityId))
                .withQueryStringParameter("appId", "registeredUserApiKey");
    }
}